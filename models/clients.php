<?php
	function indexModel(){
		View::set("index");
	}

	function pendingModel(){
		View::setVariable("categories", Repairs::$categories);
		View::setVariable("repairs", Repairs::getTable());
		View::set("pending");
	}
?>