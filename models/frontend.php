<?php
	function indexModel(){
		$cms = new CMS("frontend");

		View::setVariable("cms", $cms);
		View::set("index", "frontend");
	}

	function featuresModel(){
		$cms = new CMS("frontend");

		View::setVariable("cms", $cms);
		View::set("features", "frontend");
	}

	function pricingModel(){
		$cms = new CMS("frontend");

		View::setVariable("cms", $cms);
		View::set("pricing", "frontend");
	}

	function aboutModel(){
		$cms = new CMS("frontend");

		View::setVariable("cms", $cms);
		View::set("about", "frontend");
	}
?>
