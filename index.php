<?php
	require("../core/index.php");

	Main::init(array(
		"client"  =>
			"bts",

		"modules" => array(
			"messaging",
			"trading",
			"frontend"
		),

		"pages" => array(
			"home" => array(
				"label" => "Home",
				"icon"  => "home",
				"sub"   => array(
					"index" => "Overview"
				)
			),
			"clients" => array(
				"label" => "Clients",
				"icon"  => "user",
				"sub"   => array(
					"index"   => "Overview",
					"add"     => "Add",
					"pending" => "Pending"
				)
			),
			"settings" => array(
				"label" => "Settings",
				"icon"  => "cog",
				"sub"   => array(
					"index" => "Overview"
				)
			)
		),

		"colours" => array(
			"base" => "#e9523e"
		)
	));
?>
