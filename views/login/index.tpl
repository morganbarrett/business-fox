<div class="panel-heading">
	<img src="includes/img/logo.png" alt="logo" class="logo-img" height="200">
	</div>

<div class="panel-body">
	<form action="" method="post" class="form-horizontal">
		<input type="hidden" name="control" value="login">

		<div class="login-form">
			<if true="error">
				<div class="form-group">
					Incorrect username or password.
				</div>
			</if>

			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon">
						<i class="icon s7-user"></i>
					</span>

					<input type="text" name="username" placeholder="Username" class="form-control">
				</div>
			</div>

			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon">
						<i class="icon s7-lock"></i>
					</span>

					<input type="password" name="password" placeholder="Password" class="form-control">
				</div>
			</div>

			<div class="form-group login-submit">
				<button type="submit" class="btn btn-primary btn-lg" data-dismiss="modal">Log me in</button>
			</div>

			<div class="form-group footer row">
				<div class="col-xs-6">
					<a href="#">Forgot Password?</a>
				</div>

				<div class="col-xs-6 remember">
					<label for="remember">Remember Me</label>
					<div class="am-checkbox">
						<input type="checkbox" id="remember">
						<label for="remember"></label>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
