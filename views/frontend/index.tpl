<function call="outputHeader">
    <var set="class">full</var>
    <var set="label">Business Fox</var>
    <var set="description">
        <p>
            A business management web application suite,
            custom tailored to suit your businesses specific needs.
        </p>

        <function call="outputButton">
            <var set="label">Find Out More</var>
            <var set="link">#about</var>
            <var set="colour">primary</var>
        </function>
    </var>
</function>

<function call="outputSection">
    <var set="id">about</var>
    <var set="colour">primary</var>
    <var set="label">We've got what you need!</var>
    <var set="description">
        <p class="text-faded">
            Business Fox and it's extensive feature catalog is easily adaptable,
            allowing us to write custom applications to meet your businesses specific needs and requirements,
            more quickly and at a lower price than most other development firms.
        </p>
    </var>
</function>

<function call="outputSection">
    <var set="colour">light</var>
    <var set="label">At Your Service</var>
    <var set="body">
        <function call="outputRow">
            <var set="items">
                <hide>
                    <var set="label">Customer Relations</var>
                    <var set="description">
                        Our software allows businesses to connect with customers easily,
                        we offer instant messaging between you and your customers and
                        an automated email, templated email system.
                    </var>
                </hide>
                <hide>
                    <var set="label">Improved Efficiency</var>
                    <var set="description">
                        Our software aims to improve the efficiency within a team and to provide a
                        centralised location for all information within a business,
                        which can help to take some of the slack off your business.
                    </var>
                </hide>
                <hide>
                    <var set="label">Manage Anywhere</var>
                    <var set="description">
                        All our software and data is securely hosted in the cloud,
                        so that it can be accessed and managed from anywhere in the world
                        with an internet connection.
                    </var>
                </hide>
                <hide>
                    <var set="label">Up to Date</var>
                    <var set="description">
                        Bug fixes and new features are updated instantly to all our clients,
                        so your software is always up to date. New feautures are added regluarly
                        and new modules can be added at any point.
                    </var>
                </hide>
            </var>
        </function>
    </var>
</function>

<function call="outputSection">
    <var set="colour">dark</var>
    <var set="label">Why you should choose us</var>
    <var set="description">
        <p class="text-faded">
            Off the shelf for managing businesses quite often fails to meet the needs of businesses.
            Small businesses are especially affected as steep learning curves to use and setup such
            pieces of software are time consuming and expensive. Custom software can fix these problems,
            however this can often take up to a year to develop and can be very expensive.
        </p>
        <p class="text-faded">
            The advantage of Business fox is that you get quality, well tested software,
            that is customised to have all the feautres your business requires and to
            perfectly match your businesses workflow, but without the high cost and long
            development period.
        </p>
    </var>
</function>

<function call="outputSection">
    <var set="id">portfolio</var>
    <var set="class">no-padding</var>
    <var set="noLabel">true</var>
    <var set="body">
        <function call="outputGrid">
            <var set="items">
                <hide>
                    <var set="label">Gadget Repair Shops</var>
                    <var set="description">
                        Improve your web presence with a custom designed front end web site,
                        which utilises our in house content managment system which
                        allows you to edit and update your website content at any time.
                    </var>
                </hide>
                <hide>
                    <var set="label">Vehicle Garages</var>
                    <var set="description">
                        Add instant messaging between your employees and to your customers,
                        to improve your businesses workflow to allow your employees to help
                        your customers instantly.
                    </var>
                </hide>
                <hide>
                    <var set="label">Barbers / Hair Salons</var>
                    <var set="description">
                        Manage services your business offers and track customers orders,
                        holds information around individual orders and automates certain tasks,
                        to improve the efficiency of fufilling orders.
                    </var>
                </hide>
                <hide>
                    <var set="label">Services Companies</var>
                    <var set="description">
                        Keep a track on your businesses finances, including VAT reports
                        for businesses, tracking sales and payments from customers
                        and invoices.
                    </var>
                </hide>
                <hide>
                    <var set="label">Trading Companies</var>
                    <var set="description">
                        Plan and record appointments, employee work hours, and more
                        from within your planning module.
                    </var>
                </hide>
                <hide>
                    <var set="label">Many More</var>
                    <var set="description">
                        Track statistics and trends such as employee performance,
                        sales reports, customer conversion rates and other statistics
                        acociated with installed modules.
                    </var>
                </hide>
            </var>
        </function>
</function>

<function call="outputSection">
    <var set="id">features</var>
    <var set="colour">primary</var>
    <var set="label">See more about our features</var>
    <var set="description">
        <p class="text-faded">
            We are continuously adding new modules and updating existing ones,
            so that even after your custom software is written it will constinue to improve.
            New modules can be easily added at any time to improve your software.
        </p>
        <function call="outputButton">
            <var set="colour">default</var>
            <var set="label">See more</var>
        </function>
    </var>
</function>

<function call="outputSection">
    <var set="id">contact</var>
    <var set="colour">light</var>
    <var set="label">Let's Get In Touch!</var>
    <var set="description">
        Ready to update your business with us?
        That's great! Give us a call, drop us a text or send us an email
        and we will get back to you as soon as possible!
    </var>
    <var set="body">
        <div class="col-lg-4 col-lg-offset-2 text-center">
            <function call="icon">earphone</function>
            <p>07794576722</p>
        </div>
        <div class="col-lg-4 text-center">
            <function call="icon">envelope</function>
            <p><a href="mailto:your-email@your-domain.com">morganbarrett@icloud.com</a></p>
        </div>
    </var>
</function>
