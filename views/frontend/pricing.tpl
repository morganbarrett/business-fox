<function call="outputHeader">
    <var set="class">pricingImg</var>
    <var set="label">Pricing</var>
</function>

<function call="outputSection">
    <var set="id">about</var>
    <var set="colour">primary</var>
    <var set="label">Continuously updated</var>
    <var set="description">
        <p class="text-faded">
            BTS and it's extensive feauture catalogue allows us to write a custom application for your business needs quickly and at a reasonable price.
        </p>
        <table class="table pricingTable">
            <tr>
                <th>1 - 10</th>
                <th>11 - 50</th>
                <th>51 - 200</th>
                <th>200+</th>
            </tr>
            <tr>
                <td>users</td>
                <td>users</td>
                <td>users</td>
                <td>users</td>
            </tr>
            <tr>
                <td>£10.00 per user monthly</td>
                <td>£8.00 per user monthly</td>
                <td>£6.00 per user monthly</td>
                <td>£4.00 per user monthly</td>
            </tr>
        </table>
    </var>
</function>

<function call="outputSection">
    <var set="id">about</var>
    <var set="colour">dark</var>
    <var set="label">Continuously updated</var>
    <var set="description">
        <p>Add Ons</p>

        <table class="table pricingTable">
            <tr>
                <th>Frontend</th>
                <th>Messaging</th>
                <th>Trading</th>
                <th>Messaging</th>
            </tr>
            <tr>
                <td>Frontend gives your business a web presence.</td>
                <td>Messaging improves your businesses communication both between employees and to your customers.</td>
                <td>£2.00 per user monthly</td>
                <td>£2.00 per user monthly</td>
            </tr>
            <tr>
                <td>£2.00 per user monthly</td>
                <td>£2.00 per user monthly</td>
                <td>£2.00 per user monthly</td>
                <td>£2.00 per user monthly</td>
            </tr>
        </table>
        <a href="#services" class="page-scroll btn btn-default btn-xl sr-button">Get Started!</a>
    </var>
</function>

<function call="outputSection">
    <var set="colour">primary</var>
    <var set="label">How we deliver to you</var>
    <var set="description">
    </var>
    <var set="body">
        <li>1. </li>
    </var>
</function>

<function call="outputSection">
    <var set="id">contact</var>
    <var set="colour">light</var>
    <var set="label">Let's Get In Touch!</var>
    <var set="description">
        Ready to update your business with us?
        That's great! Give us a call, drop us a text or send us an email
        and we will get back to you as soon as possible!
    </var>
    <var set="body">
        <div class="col-lg-4 col-lg-offset-2 text-center">
            <function call="icon">mobile</function>
            <p>07794576722</p>
        </div>
        <div class="col-lg-4 text-center">
            <function call="icon">email</function>
            <p><a href="mailto:your-email@your-domain.com">morganbarrett@icloud.com</a></p>
        </div>
    </var>
</function>
