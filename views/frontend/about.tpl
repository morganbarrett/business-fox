<function call="outputHeader">
    <var set="class">aboutImg</var>
    <var set="label">About</var>
</function>

<function call="outputSection">
    <var set="id">about</var>
    <var set="colour">primary</var>
    <var set="label">Continuously updated</var>
    <var set="description">
        <p class="text-faded">
            BTS and it's extensive feauture catalogue allows us to write a custom application for your business needs quickly and at a reasonable price.
        </p>
    </var>
</function>

<function call="outputSection">
    <var set="id">contact</var>
    <var set="colour">light</var>
    <var set="label">Let's Get In Touch!</var>
    <var set="description">
        Ready to update your business with us?
        That's great! Give us a call, drop us a text or send us an email
        and we will get back to you as soon as possible!
    </var>
    <var set="body">
        <div class="col-lg-4 col-lg-offset-2 text-center">
            <function call="icon">mobile</function>
            <p>07794576722</p>
        </div>
        <div class="col-lg-4 text-center">
            <function call="icon">email</function>
            <p><a href="mailto:your-email@your-domain.com">morganbarrett@icloud.com</a></p>
        </div>
    </var>
</function>
