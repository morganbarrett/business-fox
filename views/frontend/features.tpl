<function call="outputHeader">
    <var set="class">featuresImg</var>
    <var set="label">Features</var>
</function>

<function call="outputSection">
    <var set="id">about</var>
    <var set="colour">dark</var>
    <var set="label">Continuously updated</var>
    <var set="description">
        <p class="text-faded">
            All of our clients software runs on a shared code base,
            which means that new features and bug fixes are added to
            all our clients sites simultaniously.
        </p>
    </var>
</function>

<function call="outputSection">
    <var set="id">portfolio</var>
    <var set="class">no-padding</var>
    <var set="noLabel">true</var>
    <var set="body">
        <function call="outputGrid">
            <var set="items">
                <hide>
                    <var set="label">Frontend</var>
                    <var set="description">
                        Improve your web presence with a custom designed front end web site,
                        which utilises our in house content managment system which
                        allows you to edit and update your website content at any time.
                    </var>
                </hide>
                <hide>
                    <var set="label">Communication</var>
                    <var set="description">
                        Add instant messaging between your employees and to your customers,
                        to improve your businesses workflow to allow your employees to help
                        your customers instantly.
                    </var>
                </hide>
                <hide>
                    <var set="label">Workflow</var>
                    <var set="description">
                        Manage services your business offers and track customers orders,
                        holds information around individual orders and automates certain tasks,
                        to improve the efficiency of fufilling orders.
                    </var>
                </hide>
                <hide>
                    <var set="label">Accounts</var>
                    <var set="description">
                        Keep a track on your businesses finances, including VAT reports
                        for businesses, tracking sales and payments from customers
                        and invoices.
                    </var>
                </hide>
                <hide>
                    <var set="label">Planning</var>
                    <var set="description">
                        Plan and record appointments, employee work hours, and more
                        from within your planning module.
                    </var>
                </hide>
                <hide>
                    <var set="label">Analysis</var>
                    <var set="description">
                        Track statistics and trends such as employee performance,
                        sales reports, customer conversion rates and other statistics
                        acociated with installed modules.
                    </var>
                </hide>
            </var>
        </function>
</function>

<function call="outputSection">
    <var set="colour">primary</var>
    <var set="label">Check out our pricing!</var>
    <var set="description">
        <function call="outputButton">
            <var set="colour">default</var>
            <var set="label">See prices</var>
        </function>
    </var>
</function>

<function call="outputSection">
    <var set="id">contact</var>
    <var set="colour">light</var>
    <var set="label">Let's Get In Touch!</var>
    <var set="description">
        Ready to update your business with us?
        That's great! Give us a call, drop us a text or send us an email
        and we will get back to you as soon as possible!
    </var>
    <var set="body">
        <div class="col-lg-4 col-lg-offset-2 text-center">
            <function call="icon">mobile</function>
            <p>07794576722</p>
        </div>
        <div class="col-lg-4 text-center">
            <function call="icon">email</function>
            <p><a href="mailto:your-email@your-domain.com">morganbarrett@icloud.com</a></p>
        </div>
    </var>
</function>
