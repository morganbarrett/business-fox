<div>
	<div class="btn-group" role="group">
		<button type="button" class="btn btn-primary">All</button>
		<button type="button" class="btn btn-default">Retail</button>
		<button type="button" class="btn btn-default">Trade</button>
		<button type="button" class="btn btn-default">Schools</button>
	</div>
</div>

<function call="tabs">
	<var set="tabs">
		<foreach values="categories">
			<div>
				<var set="label" copy="value"></var>
			</div>
		</foreach>
	</var>

	<var set="bodies">
		<foreach values="categories">
			<function call="outputTable">
				<var set="filterName">category</var>
				<var set="filter" copy="value"></var>
				<var set="table" copy="repairs"></var>
			</function>
		</foreach>
	</var>
</function>